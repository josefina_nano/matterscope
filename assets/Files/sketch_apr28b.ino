#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
int ThermistorPin = A0;
int ThermistorPin1 = A1;

int Vo1;
float R11 = 10000;
float logR21, R21, T1;
float c11 = 1.009249522e-03, c21 = 2.378405444e-04, c31 = 2.019202697e-07;
float C1;

int Vo;
float R1 = 10000;
float logR2, R2, T;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;
float C;



void setup() {
Serial.begin(9600);
// set up the LCD's number of columns and rows: 
lcd.begin(16, 2);
// Print a message to the LCD.
//lcd.setCursor(0,1);
//lcd.write("JiMA.in");
lcd.print("TEMP1:");
lcd.setCursor(8,0);
lcd.print("TEMP2:");

}

void loop() {

// Temperatura del sensor 1
  Vo = analogRead(ThermistorPin);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  T = T - 273.15;
  T = (T * 9.0)/ 5.0 + 32.0; 
  C = (T - 32) * 5/9;

  // Temperatura del sensor 2
  Vo1 = analogRead(ThermistorPin1);
  R21 = R11 * (1023.0 / (float)Vo1 - 1.0);
  logR21 = log(R21);
  T1 = (1.0 / (c11 + c21*logR21 + c31*logR21*logR21*logR21));
  T1 = T1 - 273.15;
  T1 = (T1 * 9.0)/ 5.0 + 32.0; 
  C1 = (T1 - 32) * 5/9;
  
  Serial.print("Temp1:"); 
  Serial.print(C);

  Serial.print("Temp2:");
  Serial.print(C1);
  
  //Serial.println(" C°"); 
  lcd.setCursor(0,1);
  lcd.print(C);
  lcd.setCursor(6,1);
  lcd.print("C");

  lcd.setCursor(8,1);
  lcd.print(C1);
  lcd.setCursor(14,1);
  lcd.print("C");

  /*if( C > 28){

  lcd.setCursor(8,1);
  lcd.print("CONTACT");
  }
  else{
  lcd.setCursor(8,1);
  lcd.print("       ");
  }*/
  


  delay(500);
}
