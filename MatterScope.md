# MatterScope

## Concept
The Matterscope, is a device, that in analogy to Telescopes and Microscopes, situates the human body to enable the observation of biomaterials through the senses. The general aim of the project is to enable and register interactions between the human body and another body, in this case biomaterial, that is being carefully observed and registered in its first days for shrinkages and deformations.

## Purpose 
The matterscope is a delimited space that situates the observed matter and the observer. The observed matter and the observer have a sensor to measure body temperature. The space is being measured for environmental temperature and humidity, all these sensors generate data that will later be crossed with a visual registration of the interaction. Specifically, throught the temperature log, the device registrates when the human body comes into contact with the non-human body through the sense of touch, and how this affects the matter.

## Plan & Execution
We started thinking that we would monitor the pulse of the human body in contact with the biomaterial. And planning to measure environmental temperatura and humidity. On the hardware, we started by defining the dimensions of the matterscope and how we were going to build it. In this case: a wooden structure with translucent walls (to be able to apply light from the outside) with a 1 x 1cm grid (to be able to observe visually the deformation of the biomaterial). We also took into account where we would place the electronics, for this reason we gave the base an 8cm extension. The electronics would be: 2 temperature sensors that would activate through 'touch'(thermistor), one to measure the human body temperature and the other to measure the temperature of the biomaterial. We also added an environmental temperature and humidity sensor inside the matterscope. At the same time we had the idea to make a small piece for the electronics with casting & molding techniques. This way the temperature sensor for the matter can be placed touching only the matter, without interference of the human body.

## Design & Fabrication
Design / 3D Models:
- Render of the structure

<img src= "assets/Photos/01_Render_box.PNG">

- Render of the 'clip' piece for molding & casting, to hold the Thermistor sensor

<img src= "assets/Photos/05_Render_Clips_y_molding.JPG">

Fabrication / Hand's on
- Structure  and Grid
The structure was built using 2,6 x 2,6cm wood scraps. The whole structure was given its dimension according to the A3 size, from the printing of the 1x1 cms grid, to facilitate this side and top panels being interchangeble, to plain white for example. Which would make the matterscope also a functional lightbox. for the simple registration of future samples.

- Cooking agar-agar biomaterial
Agar Agar Recipe:
• 300 ml of water
• 10 gr of agar agar
• 16 ml of glycerin
Instructions : Add the water and agar agar to the pot in cold. Stirring always, heat at medium temperature for 2
minutes. Raise the temperature for 2 more minutes. At minute 4 add the glycerin and optional filler. The mixture should change its texture, and start to solidify in minute 6. Pour exactly in that moment is just beginning to change texture into a dense liquid.

The mold used, is a 4mm width plywood A4 size, covered with a waterproof fabric, and a 6mm plywood frame specially made for this Agar Agar recipe. This was hung from the structure with  screwed hooks and twisted wire that was bended to make a holding structure. 

<img src= "assets/Photos/fabrication_02.jpg">

- Molding & casting
We used several techniques for the molding and casting of the "clip" piece. First, the piece was directly drawn one to one scale on a notebook. This was measured with a caliper and the measures were translated to Rinoceros and 3D modelled, and then we build a solid fram around it to make a silicon mold of the negative of the piece to later make a mold in plaster of the positive. The model was 3D printed on PLA for time reasons but it could have been CNC milled on a wax block. The mixture used for the silicone mold was 50%A and 50%B of alimentary solicone. This dried during the night. And then the plaster used was for high precision, Acrylcast, mineral casting system, with a mix of 40gr of liquid and 10gr of plaster. We added some color just for fun. 

<img src= "assets/Photos/fabrication_04.jpg"> 

## Coding Logic

We develop two parallel types of impact measurement.

Impact 01: measure the interaction between body temperature and biomaterial temperature when they come into contact. We used two temperature measurement sensors through touch (thermistor) connected to an arduino uno board and an LCD to display the values.

Code:

<img src= "assets/Photos/thermistor_01.PNG">

<img src= "assets/Photos/thermistor_02.PNG">

Impact 02: measure the ambient temperature and humidity to record the deformation of the biomaterial under different environmental conditions. We used an ambient temperature and humidity sensor, together with a visual registration of the biomaterial behavior, that is being taken in a timelapse by a Go Pro camera.

Code:

<img src= "assets/Photos/DHT_sensor_1.JPG">

<img src= "assets/Photos/DHT_sensor_2.JPG">

<img src= "assets/Photos/EnvironmentLog__1_.JPG">
 
Image

<img src= "assets/Photos/electronics.jpg">

## Files

https://gitlab.com/josefina_nano/matterscope/-/tree/master/assets/Files

## Design Elements

<img src= "assets/Photos/design_elements.png">

## Final Prototype 

<img src= "assets/Photos/final_02.jpg">

<img src= "assets/Photos/final_03.jpg">

<img src= "assets/Photos/final_04.jpg">


## Iteration Process
We made mostly digital iterations for the hardware. And for the electronics several iterations. Starting with the pulse sensor that was not precise enough, and then coming up with Thermistor sensor, for touch temperature. Setting one loop for one sensor, and then adding a second Thermistor sensor for the biomaterial. We broke two DHT sensors, which can be a little fragile, and had to fix them by replacing the base board.

<img src= "assets/Photos/iteration_01.png">

<img src= "assets/Photos/pieces.jpg">

## Future Development

For future developments, the Matterscope will serve as a support for building a Vocabulary of deformations and shrinkages of different open source biomaterial recipes. The ideal development would be to build an on line repository for all the data and images, coordinated in log time. And to find different ways to document the possible interactions of human body and matter. 

## Link to individual pages
https://ines_burdiles.gitlab.io/sitio/single-fabweek12.html

https://josefina_nano.gitlab.io/mdef-website/fabacademy10.html















